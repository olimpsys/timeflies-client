<?php
$heroku = 'deploy';
?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>Time Flies</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

        <link rel="stylesheet" href="https://code.getmdl.io/1.2.1/material.indigo-pink.min.css">

        <link rel="stylesheet" href="css/timeflies.css">

        <link rel="stylesheet" href="css/details.css">


        <script src="js/jquery-3.1.1.min.js"></script>

        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAG-ngSgp99wzbqm91wnxsDyJc4KHBo4A0&libraries=places&callback=initAutocomplete" async defer></script>

        <script defer src="https://code.getmdl.io/1.2.1/material.min.js"></script>

        <script src="https://cdn.polyfill.io/v2/polyfill.min.js?features=Intl.~locale.de,Intl.~locale.fr,Intl.~locale.it,Intl.~locale.en"></script>

        <script src="js/LocationCtrl.js"></script>

        <script src="js/timeflies.js"></script>

    </head>
    <body>
        <div id="view_options" class="mdl-layout mdl-js-layout mdl-layout--fixed-header
             mdl-layout--fixed-tabs view">
            <header class="mdl-layout__header">
                <div class="mdl-layout__header-row">
                    <!-- Title -->
                    <span class="mdl-layout-title">Time Flies</span>
                </div>
                <!-- Tabs -->
                <div id="header_options" class="mdl-layout__tab-bar mdl-js-ripple-effect">
                    <a href="#step_flight" class="mdl-layout__tab is-active">
                        <i class="material-icons">flight</i>
                    </a>
                    <a href="#step_origin" class="mdl-layout__tab">
                        <i class="material-icons">directions</i>
                    </a>
                    <a href="#step_destination" class="mdl-layout__tab">
                        <i class="material-icons">local_hotel</i>
                    </a>
                </div>
            </header>
            <div class="mdl-layout__drawer">
                <span class="mdl-layout-title">Time Flies</span>
                <nav class="mdl-navigation">
                    <a class="mdl-navigation__link" href="">My trips</a>
                </nav>
            </div>
            <main id="tabs_step_options" class="mdl-layout__content">

                <section class="mdl-layout__tab-panel is-active" id="step_flight">
                    <div class="page-content">

                        <section class="section--center mdl-grid mdl-grid--no-spacing mdl-shadow--2dp">
                            <div class="mdl-card mdl-cell mdl-cell--12-col">
                                <div class="mdl-card__supporting-text">
                                    <h4>Your Flight</h4>
                                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                        <input class="mdl-textfield__input" type="text" id="flight_number" value="KL1957">
                                        <label class="mdl-textfield__label" for="flight_number">Flight number</label>
                                    </div>
                                    <br/>
                                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                        <input class="mdl-textfield__input" type="text" id="flight_date" value="Today">
                                        <label class="mdl-textfield__label" for="flight_date">When?</label>
                                    </div>
                                    <br/>
                                    <div class="luggage-wrapper">
                                        <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="luggage">
                                            <input type="checkbox" id="luggage" class="mdl-checkbox__input" checked>
                                            <span class="mdl-checkbox__label">Luggage?</span>
                                        </label>
                                    </div>

                                </div>
                                <div class="mdl-card__actions">
                                    <button class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--accent" onclick="stepFlightOK(this)">
                                        That's it
                                    </button>
                                </div>
                            </div>
                        </section>
                        <br/>
                        <section id="step_flight2" style="display: none;" class="section--center mdl-grid mdl-grid--no-spacing mdl-shadow--2dp">
                            <div id="flight_info" class="mdl-card mdl-cell mdl-cell--12-col">
                                <div class="mdl-card__title">
                                    <h2 id="flight_info_title" class="mdl-card__title-text"></h2>
                                </div>
                                <div class="mdl-card__supporting-text">
                                    <b>Duration: </b> <span id="flight_info_duration"></span>
                                    <br/>
                                    <b>Average delay: </b> <span id="flight_info_avg_delay"></span>
                                </div>
                                <div class="mdl-card__actions">
                                    <button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" onclick="stepFlightNext(this)">
                                        Next
                                    </button>
                                </div>
                            </div>
                        </section>

                    </div>
                </section>

                <section class="mdl-layout__tab-panel" id="step_origin">
                    <div class="page-content">

                        <section class="section--center mdl-grid mdl-grid--no-spacing mdl-shadow--2dp">
                            <div class="mdl-card mdl-cell mdl-cell--12-col">
                                <div class="mdl-card__supporting-text">
                                    <h4>Leaving from</h4>
                                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                        <input class="mdl-textfield__input" type="text" id="origin_location">
                                        <label class="mdl-textfield__label" for="origin_location">Departure address</label>
                                    </div>
                                    <br/>
                                    <br/>
                                    How many people?
                                    <br/>
                                    <i class="material-icons">group</i> <span id="traveler_count">1</span>
                                    <div class="traveler-count-wrapper">
                                        <input class="mdl-slider mdl-js-slider" type="range"
                                               min="1" max="10" value="1" tabindex="0"
                                               oninput="$('#traveler_count').html(this.value); current_journey.traveler_count = this.value;" onchange="$('#traveler_count').html(this.value); current_journey.traveler_count = this.value;" >
                                    </div>
                                    <br/>

                                </div>
                                <div class="mdl-card__actions">
                                    <button class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--accent" onclick="stepOriginOK(this)">
                                        Give me the options
                                    </button>
                                </div>
                            </div>
                        </section>
                        <br/>
                        <section id="step_origin2" style="display: none;" class="section--center mdl-grid mdl-grid--no-spacing mdl-shadow--2dp">
                            <div class="mdl-card mdl-cell mdl-cell--12-col">
                                <div class="mdl-card__supporting-text mdl-grid mdl-grid--no-spacing">
                                    <h4 class="mdl-cell mdl-cell--12-col">Travel Options to the airport</h4>
                                    <ul id="travel_options_origin" class="demo-list-control mdl-list mdl-cell mdl-cell--12-col">

                                    </ul>
                                </div>
                                <div class="mdl-card__actions">
                                    <button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" onclick="stepOriginNext(this)">
                                        Next
                                    </button>
                                </div>
                            </div>
                        </section>

                    </div>
                </section>

                <section class="mdl-layout__tab-panel" id="step_destination">
                    <div class="page-content">

                        <section class="section--center mdl-grid mdl-grid--no-spacing mdl-shadow--2dp">
                            <div class="mdl-card mdl-cell mdl-cell--12-col">
                                <div class="mdl-card__supporting-text">
                                    <h4>Where will you be staying?</h4>
                                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                        <input class="mdl-textfield__input" type="text" id="destination_location">
                                        <label class="mdl-textfield__label" for="destination_location">Destination Hotel or Address</label>
                                    </div>
                                </div>
                                <div class="mdl-card__actions">
                                    <button class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--accent" onclick="stepDestinationOK(this)">
                                        Give me the options
                                    </button>
                                    <button class="mdl-button mdl-js-button" onclick="stepDestinationNext(this)">
                                        Skip
                                    </button>
                                </div>
                            </div>
                        </section>
                        <br/>
                        <section id="step_destination2" style="display: none;" class="section--center mdl-grid mdl-grid--no-spacing mdl-shadow--2dp">
                            <div class="mdl-card mdl-cell mdl-cell--12-col">
                                <div class="mdl-card__supporting-text mdl-grid mdl-grid--no-spacing">
                                    <h4 class="mdl-cell mdl-cell--12-col">Travel Options from the airport</h4>
                                    <ul id="travel_options_destination" class="demo-list-control mdl-list mdl-cell mdl-cell--12-col">

                                    </ul>
                                </div>
                                <div class="mdl-card__actions">
                                    <button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" onclick="stepDestinationNext(this)">
                                        Next
                                    </button>
                                </div>
                            </div>
                        </section>

                    </div>
                </section>

            </main>
        </div>

        <div id="view_details" class="mdl-layout mdl-js-layout mdl-layout--fixed-header view" style="display: none;">
            <header class="mdl-layout__header">
                <div class="mdl-layout__header-row">
                    <!-- Title -->
                    <span class="mdl-layout-title">Time Flies</span>
                    <!-- Add spacer, to align navigation to the right -->
                    <div class="mdl-layout-spacer"></div>
                    <!-- Navigation. We hide it in small screens. -->
                    <nav class="mdl-navigation mdl-layout--large-screen-only">
                        <a class="mdl-navigation__link" href="javascript:;" onclick="showView('options')">Options</a>
                    </nav>
                </div>
            </header>
            <div class="mdl-layout__drawer">
                <span class="mdl-layout-title">Time Flies</span>
                <nav class="mdl-navigation">
                    <a class="mdl-navigation__link" href="">My Trips</a>
                    <a class="mdl-navigation__link" href="javascript:;" onclick="showView('options')">Options</a>
                </nav>
            </div>
            <main class="mdl-layout__content">
                <div class="page-content timeline-wrapper">

                    <div class="leave-notificatoin-wrapper">
                        <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect" for="leave_notification">
                            <input type="checkbox" id="leave_notification" class="mdl-switch__input" onchange="setLeaveNotification()">
                            <span class="mdl-switch__label"></span>
                        </label>
                        <span class="notify-label">Notify me</span>
                    </div>

                    <h2>Leave at <span id="timeline_leave_at"></span></h2>

                    <section id="cd-timeline"></section>

                    <h2>Arrive at <span id="timeline_arrive_at"></span></h2>

                </div>
            </main>
            <div class="timeline-emoji" onclick="showPromoDialog()"></div>
        </div>

        <div style="display: none;">

            <div id="tpl_timeline_entry">

                <div class="cd-timeline-block">
                    <div class="cd-timeline-img">
                        <i class="material-icons">ENTRY_ICON</i>
                    </div>
                    <div class="cd-timeline-content">
                        <h5>ENTRY_TEXT</h5>
                        <p>
                            <i class="material-icons">ENTRY_TIME_ICON</i>
                            ENTRY_TIME
                        </p>
                        <span class="cd-date">
                            <i class="material-icons">watch</i>
                            ENTRY_DATE_TIME
                        </span>
                    </div>
                </div>

            </div>

            <div id="tpl_travel_option_origin">
                <li class="mdl-list__item TRAVEL_OPTION_RECOMMENDED">
                    <span class="mdl-list__item-primary-content">
                        <i class="material-icons">TRAVEL_OPTION_ICON</i>
                        &nbsp;
                        TRAVEL_OPTION_DEPARTURE - TRAVEL_OPTION_ARRIVAL
                    </span>
                    <span class="mdl-list__item-secondary-action">
                        TRAVEL_OPTION_DURATION min<br/>&euro; TRAVEL_OPTION_PRICE p.p.
                        <label class="demo-list-radio mdl-radio">
                            <input type="radio" class="mdl-radio__button" name="origin_travel_option" value="TRAVEL_OPTION_ID" onclick="" />
                        </label>
                    </span>
                </li>
            </div>

            <div id="tpl_travel_option_destination">
                <li class="mdl-list__item TRAVEL_OPTION_RECOMMENDED">
                    <span class="mdl-list__item-primary-content">
                        <i class="material-icons">TRAVEL_OPTION_ICON</i>
                        &nbsp;
                        TRAVEL_OPTION_DEPARTURE - TRAVEL_OPTION_ARRIVAL
                    </span>
                    <span class="mdl-list__item-secondary-action">
                        TRAVEL_OPTION_DURATION min<br/>&euro; TRAVEL_OPTION_PRICE p.p.
                        <label class="demo-list-radio mdl-radio">
                            <input type="radio" class="mdl-radio__button" name="destination_travel_option" value="TRAVEL_OPTION_ID" onclick="" />
                        </label>
                    </span>
                </li>
            </div>

        </div>

        <dialog id="dialog_promo" class="mdl-dialog">
            <h4 class="mdl-dialog__title">You're on time</h4>
            <div class="mdl-dialog__content">
                <p>
                    How you can enjoy your <b>21 min</b> remaining:
                    <br/>
                    <img src="img/sbf-promo.png" width="240" />
                </p>
            </div>
            <div class="mdl-dialog__actions">
                <button type="button" class="mdl-button mdl-button--raised mdl-button--accent" onclick="document.getElementById('dialog_promo').close();">Check it out!</button>
                <button type="button" class="mdl-button close" onclick="document.getElementById('dialog_promo').close();">Not now</button>
            </div>
        </dialog>

        <dialog id="dialog_leave_notification" class="mdl-dialog">
            <h4 class="mdl-dialog__title">SMS notification</h4>
            <div class="mdl-dialog__content">
                <p>
                    Get an SMS when it's time to leave.
                </p>
                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                    <input class="mdl-textfield__input" type="text" id="leave_notification_phone_number">
                    <label class="mdl-textfield__label" for="leave_notification_phone_number">Phone number</label>
                </div>

            </div>
            <div class="mdl-dialog__actions">
                <button type="button" class="mdl-button mdl-button--raised mdl-button--accent" onclick="scheduleLeaveNotification(); document.getElementById('dialog_leave_notification').close();">Notify me!</button>
                <button type="button" class="mdl-button close" onclick="document.getElementById('dialog_leave_notification').close();">Cancel</button>
            </div>
        </dialog>
        
        <dialog id="dialog_security_appointment" class="mdl-dialog">
            <h4 class="mdl-dialog__title">Beat the queue!</h4>
            <div class="mdl-dialog__content">
                <p>
                    You're scheduled to be at security control at <b><span id="security_appointment_time"></span></b> 
                </p>
                <img src="img/qrcode.jpg" width="240" />
            </div>
            <div class="mdl-dialog__actions">
                <button type="button" class="mdl-button mdl-button--raised mdl-button--accent" onclick="document.getElementById('dialog_security_appointment').close();">Will be there!</button>
                <button type="button" class="mdl-button close" onclick="document.getElementById('dialog_security_appointment').close();">Cancel</button>
            </div>
        </dialog>

    </body>
</html>
