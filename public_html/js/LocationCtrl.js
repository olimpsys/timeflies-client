var geocoder, autocomplete_origin, autocomplete_destination;

function initAutocomplete() {
    geocoder = new google.maps.Geocoder;
    // Create the autocomplete object, restricting the search to geographical
    // location types.
    autocomplete_origin = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('origin_location')),
            {types: []});

    // When the user selects an address from the dropdown, populate the address
    // fields in the form.
    autocomplete_origin.addListener('place_changed', getOriginCoords);

    autocomplete_destination = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('destination_location')),
            {types: []});

    // When the user selects an address from the dropdown, populate the address
    // fields in the form.
    autocomplete_destination.addListener('place_changed', getDestinationCoords);
    
    geolocate();
}

function getOriginCoords() {
    // Get the place details from the autocomplete object.
    var place = autocomplete_origin.getPlace();
    
    current_journey.origin_location = {lat: place.geometry.location.lat(), lng: place.geometry.location.lng()};
}

function getDestinationCoords() {
    // Get the place details from the autocomplete object.
    var place = autocomplete_destination.getPlace();
    
    current_journey.destination_location = {lat: place.geometry.location.lat(), lng: place.geometry.location.lng()};
}

// Bias the autocomplete object to the user's geographical location,
// as supplied by the browser's 'navigator.geolocation' object.
function geolocate() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
            var geolocation = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };
            var circle = new google.maps.Circle({
                center: geolocation,
                radius: position.coords.accuracy
            });
            autocomplete_origin.setBounds(circle.getBounds());

            geocoder.geocode({'location': geolocation}, function (results, status) {
                if (status === 'OK') {
                    if (results[1]) {
                        $("#origin_location").val(results[1].formatted_address);
                    }
                }
            });
            
            current_journey.origin_location = geolocation;
        });
    }
}