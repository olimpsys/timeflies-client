var journeys = [];
var current_journey = {};

$(document).ready(function () {

});

function stepFlightOK(btn) {

    $(btn).prop('disabled', true);

    try {
        current_journey.luggage = $('#luggage').prop('checked');

        current_journey.flight_number = $('#flight_number').val().replace(' ', '');

        current_journey.flight_date = ($('#flight_date').val() == 'Today' ? new Date().getTime() : new Date($('#flight_date').val()).getTime());

    } catch (e) {
        current_journey.flight_date = new Date().getTime();
    }

    $.get('https://timeflies-backend.herokuapp.com/v1/flight/' + current_journey.flight_number + '/date/' + current_journey.flight_date, function (data) {
    //$.get('api/flight_info.json', function (data) {

        current_journey.flight_details = data;

        $('#flight_info_title').html(
                '<i class="material-icons">flight_takeoff</i>' +
                data.origin + '&nbsp;<br/>' +
                getHoursFromDate(data.departure_time) +
                '<i class="material-icons">flight_land</i>' +
                data.destination + '&nbsp;<br/>' +
                getHoursFromDate(data.arrival_time)
                );

        $('#flight_info_duration').html(getDurationByMinutes(data.duration));

        $('#flight_info_avg_delay').html(data.average_delay + ' min');

        $('#flight_info .mdl-card__title').get(0).style.backgroundImage = 'url(img/bg-' + (data.destination.indexOf('GUARULHOS') > -1 ? 'GRU' : 'ZRH') + '.jpg)';

        $('#step_flight2').show();


        $(btn).prop('disabled', false);

    });
}

function stepFlightNext() {

    $('a[href="#step_origin"]').get(0).click();

}

function stepOriginOK(btn) {

    $(btn).prop('disabled', true);

    $.get('https://timeflies-backend.herokuapp.com/v1/journey?' +
    //$.get('api/trip_details_origin.json?' +
            'type=departure' +
            '&origin=' + current_journey.origin_location.lat + ',' + current_journey.origin_location.lng +
            '&flight=' + current_journey.flight_number +
            '&date=' + current_journey.flight_date +
            '&luggage=' + current_journey.luggage +
            '&travelers=' + (current_journey.traveler_count || 1), function (data) {

        current_journey.origin_details = data;

        showOriginTravelOptions(data.trips);

        $('#step_origin2').show();

        $(btn).prop('disabled', false);

    });

}

function showOriginTravelOptions(travelOptions) {
    for (var t = 0; t < travelOptions.length; t++) {
        $('#travel_options_origin').append(createTravelOptionEntry(travelOptions[t], 'origin'));
    }
}

function stepOriginNext() {

    //get only selected travel option
    current_journey.origin_details.trips = current_journey.origin_details.trips.filter(function (tripObj) {
        return tripObj.trip_id == $("input[name=origin_travel_option]:checked").val();
    });

    $('a[href="#step_destination"]').get(0).click();

}

function stepDestinationOK(btn) {

    $(btn).prop('disabled', true);

    $.get('https://timeflies-backend.herokuapp.com/v1/journey?' +
    //$.get('api/trip_details_destination.json?' +
            'type=arrival' +
            '&destination=' + current_journey.destination_location.lat + ',' + current_journey.destination_location.lng +
            '&flight=' + current_journey.flight_number +
            '&date=' + current_journey.flight_date +
            '&luggage=' + current_journey.luggage +
            '&travelers=' + (current_journey.traveler_count || 1), function (data) {

        current_journey.destination_details = data;

        showDestinationTravelOptions(data.trips);

        $('#step_destination2').show();

        $(btn).prop('disabled', false);

    });

}

function showDestinationTravelOptions(travelOptions) {
    for (var t = 0; t < travelOptions.length; t++) {
        $('#travel_options_destination').append(createTravelOptionEntry(travelOptions[t], 'destination'));
    }
}

function stepDestinationNext() {

    if (current_journey.destination_details) {

        //get only selected travel option
        current_journey.destination_details.trips = current_journey.destination_details.trips.filter(function (tripObj) {
            return tripObj.trip_id == $("input[name=destination_travel_option]:checked").val();
        });

    }

    showView('details');

    showTripDetails();

}

function showTripDetails() {

    var dOrigin = current_journey.origin_details;
    var dOriginAirport = dOrigin.airport_details;
    var dFlight = current_journey.flight_details;
    var dDestination = current_journey.destination_details;
    var dDestinationAirport = dOrigin ? dOrigin.airport_details : undefined;

    var calcTimestamp = dOrigin.trips[0].arrival_time;

    var addTime = function (timeToAdd) {
        calcTimestamp += (timeToAdd * 60 * 1000);
    };

    setTripEmoji();
    checkSecurityRealTime();

    $('#timeline_leave_at').html(getHoursFromDate(dOrigin.trips[0].departure_time));

    $('#cd-timeline').append(
            createTimelineEntry(
                    dOrigin.trips[0].mode == 'transit' ? 'train' : 'local_taxi',
                    'Arrive at the airport',
                    '',
                    '',
                    calcTimestamp)
            );

    $('#cd-timeline').append(createTimelineEntry('flight_takeoff', 'Go to Terminal', dOriginAirport.time_to_terminal + ' min', 'directions_walk', calcTimestamp));
    addTime(dOriginAirport.time_to_terminal);

    if (dOriginAirport.luggage_time > 0) {
        $('#cd-timeline').append(createTimelineEntry('card_travel', 'Baggage Drop off', dOriginAirport.luggage_time + ' min', 'person_outline', calcTimestamp));
        addTime(dOriginAirport.luggage_time);
    }

    $('#cd-timeline').append(
            createTimelineEntry(
                    'security',
                    'Security Control',
                    '<span id="security_real_time">' + (isNaN(dOriginAirport.security_wait_time) ? dOriginAirport.security_wait_time : parseInt(dOriginAirport.security_wait_time)) + '</span>' + ' min' +
                    '<button type="button" class="mdl-button mdl-button--raised mdl-button--accent" style="float:right;" onclick="document.getElementById(\'dialog_security_appointment\').showModal()">Appointment</button>',
                    'people_outline',
                    calcTimestamp)
            );
    $('#security_appointment_time').html(getHoursFromDate(calcTimestamp));
    addTime(dOriginAirport.security_wait_time);

    if (dOriginAirport.passport_control_time > 0) {
        $('#cd-timeline').append(createTimelineEntry('assignment_ind', 'Passport Control', dOriginAirport.passport_control_time + ' min', 'people_outline', calcTimestamp));
        addTime(dOriginAirport.passport_control_time);
    }

    $('#cd-timeline').append(createTimelineEntry('flight_takeoff', 'Go to Gate', dOriginAirport.time_to_gate + ' min', 'directions_walk', calcTimestamp));
    addTime(dOriginAirport.time_to_gate);

    calcTimestamp = dFlight.departure_time;
    $('#cd-timeline').append(createTimelineEntry('flight', 'Flight', getDurationByMinutes(dFlight.duration), 'airline_seat_recline_normal', calcTimestamp));
    calcTimestamp = dFlight.arrival_time;

    if (dFlight.average_delay >= 10) {
        $('#cd-timeline').append(createTimelineEntry('hourglass_empty', 'Flight Delay', dFlight.average_delay + 'min', 'schedule', calcTimestamp));
        addTime(dFlight.average_delay);
    }

    if (!dDestination) {

        $('#timeline_arrive_at').html(getHoursFromDate(calcTimestamp));

    } else {

        if (dDestinationAirport.luggage_time > 0) {
            $('#cd-timeline').append(createTimelineEntry('card_travel', 'Luggage Belt', dDestinationAirport.luggage_time + ' min', 'schedule', calcTimestamp));
            addTime(dDestinationAirport.luggage_time);
        }

        if (dDestinationAirport.passport_control_time > 0) {
            $('#cd-timeline').append(createTimelineEntry('assignment_ind', 'Passport Control', dDestinationAirport.passport_control_time + ' min', 'people_outline', calcTimestamp));
            addTime(dDestinationAirport.passport_control_time);
        }

        $('#cd-timeline').append(createTimelineEntry('exit_to_app', 'Go to Exit', (dDestinationAirport.time_to_exit || 10) + ' min', 'directions_walk', calcTimestamp));
        addTime(dOriginAirport.time_to_exit);

        $('#cd-timeline').append(createTimelineEntry(dDestination.trips[0].mode == 'transit' ? 'train' : 'local_taxi', 'Leave the airport', '', '', dDestination.trips[0].departure_time));

        $('#timeline_arrive_at').html(getHoursFromDate(dDestination.trips[0].arrival_time));

    }

}

function setLeaveNotification() {
    if ($('#leave_notification').prop('checked')) {
        document.getElementById('dialog_leave_notification').showModal();
    }
}

function scheduleLeaveNotification() {
    $.get('https://timeflies-backend.herokuapp.com/v1/notify?' +
            'phone_number=' + $('#leave_notification_phone_number').val() +
            '&time=' + current_journey.origin_details.trips[0].departure_time);
}

function setTripEmoji() {
    $('.timeline-emoji').hide();

    $('.timeline-emoji')
            .addClass('worried')
            .show();

    setTimeout(function () {
        $('.timeline-emoji')
                .removeClass('worried')
                .addClass('happy')
                .addClass('mdl-badge')
                .attr('data-badge', '1');
    }, 16000);

}

function showPromoDialog() {
    if ($('.timeline-emoji').hasClass('happy')) {
        document.getElementById('dialog_promo').showModal();
    }
}

function checkSecurityRealTime() {
    
    $.get('https://timeflies-backend.herokuapp.com/v1/securitytime/' + current_journey.flight_number + '/date/' + current_journey.flight_date, function (data) {
        
        $('#security_real_time').fadeOut();
        
        var realtime = (isNaN(Number(data.security_wait_time)) ? data.security_wait_time : (parseInt(data.security_wait_time)));
        
        $('#security_real_time').html(realtime);
        
        $('#security_real_time').fadeIn();
        
        setTimeout(checkSecurityRealTime, 2000);
        
    });

}

function showView(view) {

    $('.view').parent().hide();
    $('#view_' + view).parent().show();
    $('#view_' + view).show();

}

function createTimelineEntry(icon, txt, time, iconTime, dateTime) {
    var tpl = $('#tpl_timeline_entry').html();

    tpl = tpl.replace(/ENTRY_ICON/g, icon);
    tpl = tpl.replace(/ENTRY_TEXT/g, txt);
    tpl = tpl.replace(/ENTRY_TIME_ICON/g, iconTime);
    tpl = tpl.replace(/ENTRY_TIME/g, time);
    tpl = tpl.replace(/ENTRY_DATE_TIME/g, getHoursFromDate(dateTime));

    return tpl;
}

function createTravelOptionEntry(travelOption, type) {
    var tpl = $('#tpl_travel_option_' + type).html();

    tpl = tpl.replace(/TRAVEL_OPTION_ICON/g, travelOption.mode == 'transit' ? 'train' : 'local_taxi');
    tpl = tpl.replace(/TRAVEL_OPTION_MODE/g, travelOption.mode == 'transit' ? 'Public transport' : 'Taxi');
    tpl = tpl.replace(/TRAVEL_OPTION_ID/g, travelOption.trip_id);
    tpl = tpl.replace(/TRAVEL_OPTION_DURATION/g, getDurationByMinutes(travelOption.duration));
    tpl = tpl.replace(/TRAVEL_OPTION_PRICE/g, (travelOption.price || '-'));
    tpl = tpl.replace(/TRAVEL_OPTION_DEPARTURE/g, getHoursFromDate(travelOption.departure_time));
    tpl = tpl.replace(/TRAVEL_OPTION_ARRIVAL/g, getHoursFromDate(travelOption.arrival_time));
    tpl = tpl.replace(/TRAVEL_OPTION_RECOMMENDED/g, travelOption.recommended ? 'recommended' : '');

    return tpl;
}

function getDurationByMinutes(minutes) {
    var hours = Math.floor(minutes / 60);
    var minutes = minutes % 60;

    return hours + ':' + minutes;
}

function getHoursFromDate(timestamp) {
    return new Intl.DateTimeFormat('en-en', {
        hour: "numeric",
        minute: "numeric"
    }).format(new Date(timestamp));
}